CC = gcc
CFLAGS = -Wall -O2 -c
LDFLAGS = -Wall
DEBUG = -UDEBUG
INFO = -DINFO

OBJECTS = err.o utils.o shoutcast.o commands.o
ALL = player

all: $(ALL)

%.o : %.c
	$(CC) $(CFLAGS) $(DEBUG) $(INFO) $<

$(ALL) : % : %.o $(OBJECTS)       
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	rm -f *.o $(ALL) *~ *.bak
