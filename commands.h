/*
 *    Wojciech Gizynski 264887
 */

#ifndef _COMMANDS_
#define _COMMANDS_

#define PAUSE           1
#define PAUSE_STRING    "PAUSE"
#define PLAY            2
#define PLAY_STRING     "PLAY"
#define TITLE           3
#define TITLE_STRING    "TITLE"
#define QUIT            4
#define QUIT_STRING     "QUIT"
#define NO_COMMAND      0

/* Przygotowuje moduł do obsługi komunikacji poprzez UDP */
extern void initializeUDPCommandHandling(int portNumber);

/* Odbiera pierwsze polecenie w kolejce i je analizuje;
 * w wyniku przekazywany jest numer, zgodnie z makrami PAUSE/PLAY/TITLE/QUIT 
 * Zwrócenie 0 oznacza brak otrzymanego polecenia */
extern int handleIncomingUDPCommand();

/* Ustawia jaka ma być odpowiedź na polecenie TITLE */
extern void setTitleResponse(char * str);

/* Zamknij socket uzywany do komunikacji poprzez UDP */
extern void closeUDPConnection();

#endif