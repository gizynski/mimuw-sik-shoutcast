/*
 *    Wojciech Gizynski 264887
 */

#include <stdio.h>
#include <string.h>

#include "err.h"

#define COLOR_RESET 		"\x1b[0m"
#define COLOR_BRACKET 		"\e[38;5;240m"
#define COLOR_INDEX 		"\e[38;5;240m"
#define COLOR_ASCII 		"\e[38;5;104m"
#define COLOR_CHAR 			"\e[38;5;11m"


int min (int a, int b) { 
	return a < b ? a : b; 
}

void explain(char * data, int len) {
	#ifdef DEBUG 
		int lineMax = 20;

		int processedChars = 0;
		while(processedChars < len) {
			int limit = processedChars + lineMax;
			int i;
			for (i = processedChars; i < limit; i++) {
				fprintf(stderr, "%s[%s", COLOR_BRACKET, COLOR_INDEX);
				fprintf(stderr, "%04d", i);
				fprintf(stderr, "%s]%s", COLOR_BRACKET, COLOR_RESET);
			}
			fprintf(stderr, "\n");
			for (i = processedChars; i < limit; i++) {
				fprintf(stderr, "%s[%s", COLOR_BRACKET, COLOR_ASCII);
				int charCode = (int) data[i];
				charCode = charCode < 0 ? (127 - charCode) : charCode;
				fprintf(stderr, "%04d", charCode);
				fprintf(stderr, "%s]%s", COLOR_BRACKET, COLOR_RESET);
			}
			fprintf(stderr, "\n");
			for (i = processedChars; i < limit; i++) {
				char str[10];
				sprintf(str, "%c", data[i]);
				int charCode = (int) data[i];
				if (charCode >= 0 && (charCode < 32 || charCode == 127)) {
					switch(charCode) {
						case 0: sprintf(str, "\\0"); break;
						case 9: sprintf(str, "\\t"); break;
						case 10: sprintf(str, "\\n"); break;
						case 11: sprintf(str, "!VT"); break;
						case 13: sprintf(str, "\\r"); break;
						case 14: sprintf(str, "!SO"); break;
						case 15: sprintf(str, "!SI"); break;
						case 127: sprintf(str, "!DEL"); break;
						default: sprintf(str, "!!!"); break;
					}
				}
				fprintf(stderr, "%s[%s", COLOR_BRACKET, COLOR_CHAR);
				fprintf(stderr, "%4s", str);
				fprintf(stderr, "%s]%s", COLOR_BRACKET, COLOR_RESET);
			}
			fprintf(stderr, "\n\n");
			processedChars += lineMax;
		}
	#endif
}