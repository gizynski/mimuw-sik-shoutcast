/*
 *    Wojciech Gizynski 264887
 */

#ifndef _UTILS_
#define _UTILS_

/* Zwraca mniejsza z dwoch liczb */
extern int min (int a, int b);

/* Wizualizuje bufor danych na stdout */
extern void explain(char * data, int len);

#endif 