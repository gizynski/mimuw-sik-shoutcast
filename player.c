/*
 *    Wojciech Gizynski 264887
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <ctype.h>
// #include <sys/wait.h>
// #include <fcntl.h>   

#include "err.h"
#include "commands.h"
#include "shoutcast.h"

#define PORT_MIN_NUMBER         1025
#define PORT_MAX_NUMBER         65534
#define STRING_LIMIT            256

int play = 1;
char * argumentHost;
char * argumentPath;
int argumentRPort;
int argumentMPort;
int argumentMetadata;
FILE *argumentFileDestination; 

int isProperPortNumber(char * str, int minPortNumber) {
    int result = 0;
    int i;
    for (i = 0; i < strlen(str); i++) {
        if (!isdigit(str[i])) {
            debug(2, "character '%c' on index %d is not a digit", str[i], i);
            return 0;
        }
    }
    int portInt = atoi(str);
    if (portInt == 0) {
        debug(2, "either not an int value or 0 was supplied for port number: %s", str);
    } else if (portInt < minPortNumber || portInt > PORT_MAX_NUMBER) {
        debug(2, "port number %d was out of allowed range [%d-%d]", portInt, PORT_MIN_NUMBER, PORT_MAX_NUMBER);
    } else {
        debug(2, "proper port number %d", portInt);
        result = 1;
    }
    return result;
}

int validateMetadataArgument(char * str) {
    if (strcmp("yes", str) == 0){
        debug(2, "demanding metadata argument set to 1");
        return 1;
    }
    if (strcmp("no", str) == 0){
        debug(2, "demanding metadata argument set to 0");
        return 0;
    }
    fatal("md has invalid value, only yes/no is allowed");
    return -1;
}

void prepareFileDescriptor(char * fileString) {
    if (strcmp("-", fileString) == 0) {
        argumentFileDestination = stdout;
    } else {
        argumentFileDestination = fopen(fileString, "w");
        if (argumentFileDestination == NULL) {
            syserr("opening file %s", fileString);
        } else {
            debug(2, "file %d was opened succesfully", fileString);
        }
    }
}

void readProgramArguments(int argc, char *argv[]) {
    debug(0, "reading program arguments");

    if (argc != 7) {
        fatal("Usage: %s <host> <path> <r-port> <file> <m-port> <md>", argv[0]);
    }

    debug(1, "reading host argument, shoutcast server host");
    argumentHost = argv[1];

    debug(1, "reading path argument, path for the GET request to shoutcast server");
    argumentPath = argv[2];

    debug(1, "reading r-port, port of tcp shoutcast");
    if (!isProperPortNumber(argv[3], 1)) {
        fatal("r-port has invalid value");
    } else {
        argumentRPort = atoi(argv[3]);
    };

    debug(1, "reading file, destination to store data (use - for stdout)");
    prepareFileDescriptor(argv[4]);

    debug(1, "reading m-port, port for udp communication");
    if (!isProperPortNumber(argv[5], PORT_MIN_NUMBER)) {
        fatal("m-port has invalid value");
    } else {
        argumentMPort = atoi(argv[5]);
    };

    debug(1, "reading md, should the metadata be processed");
    argumentMetadata = validateMetadataArgument(argv[6]);
}

void exitProgram() {
    debug(0, "exiting program gracefullly");
    if (argumentFileDestination != stdout) {
        if (fclose(argumentFileDestination) != 0) {
            syserr("closing destination file");
        }
        debug(1, "destination file was closed");
    } else {
        debug(1, "destination was stdout, no file to close");
    }
    closeTCPConnection();
    closeUDPConnection();
    debug(1, "all done, see you");
    exit(0);
}

void setPlaying(int shouldTheMusicBeStreamed) {
    play = shouldTheMusicBeStreamed;
}

void doAction(int cmd) {
    switch(cmd) {
        case PAUSE:   setPlaying(0); break;
        case PLAY:    setPlaying(1); break;
        case QUIT:    exitProgram(); break;
    }
}

int main(int argc, char *argv[]) {
    debug(0, "program started");
    readProgramArguments(argc, argv);
    initializeUDPCommandHandling(argumentMPort);
    initializeTCPShoutcastStreaming(argumentHost, argumentPath, argumentFileDestination, argumentRPort, argumentMetadata);

    debug(0, "entering infinite loop");
    for (;;) {
        debug(1, "loop iteration start");
        if (handleIncomingTCPShoutcastData(play) == 0) {
            exitProgram();
        }
        int command = handleIncomingUDPCommand();
        doAction(command);
        debug(2, "loop iteration end");
    }

    exitProgram();
}
