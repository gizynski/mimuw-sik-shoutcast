/*
 *    Wojciech Gizynski 264887
 */
 
#ifndef _ERR_
#define _ERR_

/* wypisuje informacje o blednym zakonczeniu funkcji systemowej 
i konczy dzialanie */
extern void syserr(const char *fmt, ...);

/* wypisuje informacje o bledzie i konczy dzialanie */
extern void fatal(const char *fmt, ...);

/* wypisuje informacje do debugowania, uzywam dla wlasnych potrzeb
 * zeby wlaczyc debug, zamieniamy w Makefile -UDEBUG na -DDEBUG
 * przy zmianie trzeba tez zrobic make clean */
extern void debug(int level, const char *fmt, ...);

/* wypisuje informacje dla uzytkownika programu,
 * zeby wlaczyc info, zamieniamy w Makefile -UINFO na -DINFO
 * przy zmianie trzeba tez zrobic make clean */
extern void info(int level, const char *fmt, ...);

#endif
