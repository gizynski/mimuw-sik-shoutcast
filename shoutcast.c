/*
 *    Wojciech Gizynski 264887
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "err.h"
#include "utils.h"
#include "commands.h"

#define BUFFER_SIZE 		5000
#define MAX_HEADER 			5000
#define REQUEST_TEMPLATE	"GET %s HTTP/1.0\r\nHost: %s\r\nUser-Agent: MIMUW\r\nIcy-MetaData: %d\r\n\r\n"
#define ICY_OK_STATUS		"ICY 200 OK"
#define HTTP1_0_OK_STATUS		"HTTP/1.0 200 OK"
#define HTTP1_1_OK_STATUS		"HTTP/1.1 200 OK"

#define METADATA_CHUNK		16
#define MAX_BYTE_VALUE		255
#define METADATA_LIMIT		MAX_BYTE_VALUE * METADATA_CHUNK
#define STRING_LIMIT		256

char * settingHost;
char settingPort[10];
char * settingPath;
int  settingMetaData;
long settingMetaDataInterval;
long bytesReceivedModAudioChunk = 0;
char streamTitle[METADATA_LIMIT];
int forwardingData = 1;
FILE * settingDataDestination;

int handleIncomingTCPShoutcastData();

void writeBytesToDestination(char * data, int nbytes) {
	if (nbytes < 0) {
		fatal("trying to write negative number of bytes");
	}
	if (settingMetaData) {
		bytesReceivedModAudioChunk += nbytes;
		if (bytesReceivedModAudioChunk > settingMetaDataInterval) {
			fatal("this shouldn't have happened - metadata wasn't properly handled");
		}
	}
	if (forwardingData) {
		debug(4, "printing %d data bytes to the destination", nbytes);
		int bitsWrittenToDestination = fwrite(data, sizeof(char), nbytes, settingDataDestination);
		int expectedAmountOfBitsWritten = sizeof(char) * nbytes;
		if (bitsWrittenToDestination != expectedAmountOfBitsWritten) {
			syserr("writing to destination, should be %d bits, but was %d", expectedAmountOfBitsWritten, bitsWrittenToDestination);
		}
	} else {
		debug(4, "playing is paused, so not writing any data right now");
	}
}

char metaData[METADATA_LIMIT + 1];
int metaDataLength = 0;
int metadataBytesLeftToProcess = 0;
int metadataBytesAlreadyProcessed = 0;

int extractStreamTitleValueFromMetadata() {
	debug(7, "extracting streamTitle value from metadata");
	char * pch;
	pch = strchr(metaData, '\'');
	if (pch == NULL) {
		debug(7, "no \' characters in the metadata block, leaving the function");
		return 1;
	}
	int firstApostrophIndex = pch - metaData;
	int lastApostrophIndex;
	int i;
	for (i = 0; i < 3; i++) {
		pch = strrchr(metaData, '\'');
		if (pch == NULL) {
			debug(7, "no \' characters left in the metadata block, leaving the function");
			return 1;
		}
		lastApostrophIndex = pch - metaData;
		debug(7, "ignoring last occurence of \' at position %d", lastApostrophIndex);
		metaData[lastApostrophIndex] = '\0';
	}
	debug(7, "we are interested in characters in range [%d-%d]", firstApostrophIndex, lastApostrophIndex);
	if (lastApostrophIndex <= firstApostrophIndex) {
		debug(7, "problem with apostroph characters, avoiding buffer overflow, leaving the function");
		return 1;
	}
	int bytesToCopyBetweenStrings = lastApostrophIndex - firstApostrophIndex - 1;
	strncpy(streamTitle, metaData + firstApostrophIndex + 1, bytesToCopyBetweenStrings);
	streamTitle[bytesToCopyBetweenStrings] = '\0';
	debug(7, "extracted streamTitle is '%s'", streamTitle);
	setTitleResponse(streamTitle);
	return 0;
}

int handleMetadata(char * data, int nbytes, int startingByte) {
	debug(5, "handling metadata");
	int resultBytesFromTheChunkWithMetadata = 0;

	if (metadataBytesLeftToProcess == 0) {
		debug(6, "just starting with this metadata segment");
		int howManySegments = data[startingByte];
		if (howManySegments < 0) {
			howManySegments = 127 - howManySegments;
		}
		metaDataLength = howManySegments * METADATA_CHUNK;
		memset(metaData, 0, sizeof(metaData));
		metadataBytesLeftToProcess = metaDataLength;
		metadataBytesAlreadyProcessed = 0;
		resultBytesFromTheChunkWithMetadata += 1;
		if (metaDataLength == 0) {
			debug(6, "empty metadata segment");
			bytesReceivedModAudioChunk = 0;
			return resultBytesFromTheChunkWithMetadata;
		}
	} else {
		debug(6, "continuing work on metadata segment");
	}
	debug(6, "metadata: length %d, leftToProcess %d, alreadyProcessed %d", metaDataLength, metadataBytesLeftToProcess, metadataBytesAlreadyProcessed);

	int bytesLeftFromThisChunk = nbytes - startingByte - 1;
	int bytesForMetadataFromThisChunk = min(bytesLeftFromThisChunk, metadataBytesLeftToProcess);
	debug(6, "bytes for metadata from this chunk %d", bytesForMetadataFromThisChunk);
	memcpy(&metaData[metadataBytesAlreadyProcessed], &data[startingByte + 1], bytesForMetadataFromThisChunk);
	metadataBytesAlreadyProcessed += bytesForMetadataFromThisChunk;
	metadataBytesLeftToProcess -= bytesForMetadataFromThisChunk;

	if (metadataBytesLeftToProcess == 0) {
		debug(6, "metaData: %s", metaData);
		bytesReceivedModAudioChunk = 0;
		// explain(metaData, metaDataLength);
		extractStreamTitleValueFromMetadata();
	}
	resultBytesFromTheChunkWithMetadata += bytesForMetadataFromThisChunk;
	return resultBytesFromTheChunkWithMetadata;
}

void handleAudioDataChunk(char * data, int nbytes) {
	if (settingMetaData) {
		debug(4, "verify if the metadata is in this chunk");
		debug(5, "bytes received before %ld, interval %ld, this chunk %d", bytesReceivedModAudioChunk, settingMetaDataInterval, nbytes);
		// sleep(1);
		if (bytesReceivedModAudioChunk + nbytes > settingMetaDataInterval) {
			debug(5, "metadata is in this chunk");
			// explain(data, nbytes);
			long bytesBeforeMetadata = settingMetaDataInterval - bytesReceivedModAudioChunk;
			debug(5, "bytes before metadata %d", bytesBeforeMetadata);
			writeBytesToDestination(data, bytesBeforeMetadata);
			int bytesOccupiedByMetadata = handleMetadata(data, nbytes, bytesBeforeMetadata);
			int bytesLeftWithAudioData = nbytes - bytesBeforeMetadata - bytesOccupiedByMetadata;
			if (bytesLeftWithAudioData > 0) {
				debug(5, "there is some more data after metadata, handling it now");
				handleAudioDataChunk(data + bytesBeforeMetadata + bytesOccupiedByMetadata, bytesLeftWithAudioData);
			}
		} else {
			writeBytesToDestination(data, nbytes);
		}
	} else {
		writeBytesToDestination(data, nbytes);
	}
}

int headerProcessed = 0;
char tempHeaderBuffer[MAX_HEADER];
int bytesReceivedAndPutForHeader = 0;
int bytesEqualToHeaderEnding = 0;

void parseDataFromShoutcastHeader(char * header, int len) {
	debug(4, "parsing data from the response header");
	debug(4, "header: \n%s", header);
	// explain(header, len);

	char* responseLine;
	char* delimeters = "\r\n";

	int metadataConfirmedICY = 0;

	responseLine = strtok(header, delimeters);
	debug(5, "1st responseLine: %s", responseLine);
	if (strcmp(responseLine, ICY_OK_STATUS) != 0) {
		debug(5, "shoutcast server didn't respond with %s", ICY_OK_STATUS);
		if (strcmp(responseLine, HTTP1_0_OK_STATUS) != 0 && strcmp(responseLine, HTTP1_1_OK_STATUS) != 0) {
			fatal("shoutcast server didn't respond with OK status", ICY_OK_STATUS);
		}
	} else {
		metadataConfirmedICY = 1;
		debug(5, "assuming this is proper ICY header");
	}

	debug(5, "looking for icy-metaint information");
	int metadataIntervalWasScanned = 0;
	int bitrate = 0;
	while ((responseLine = strtok(NULL, delimeters)) != NULL) {
		debug(5, "next: %s", responseLine);
		if (sscanf(responseLine, "icy-metaint:%ld", &settingMetaDataInterval) == 1){
			debug(5, "found icy-metaint value - %ld", settingMetaDataInterval);
			if (!settingMetaData) {
				fatal("icy-metaint was sent by the server even though request was for NO metadata");
			}
			metadataIntervalWasScanned = 1;
		}
		if (sscanf(responseLine, "icy-br:%d", &bitrate) == 1){
			debug(5, "found icy-br value - %d", bitrate);
			debug(5, "assuming this is proper ICY header");
			metadataConfirmedICY = 1;
		}
	}
	if (settingMetaData && !metadataIntervalWasScanned) {
		info(5, "server didn't provide icy-metaint value, assuming that metadata won't show up");
		settingMetaData = 0;
	}
	if (!metadataConfirmedICY) {
		fatal("shoutcast server didn't respond with proper ICY heading");
	}
	debug(5, "finished parsing header");
}

void processReceivedData(char * data, int nbytes) {
	debug(2, "processing received tcp data");
    if (!headerProcessed) {
    	debug(3, "header is not processed, chars matching to the ending %d", bytesEqualToHeaderEnding);
    	int i;
    	for (i = 0; i < nbytes; i++) {
			char c = data[i];
			debug(4, "processing char %c", c);
			
			tempHeaderBuffer[bytesReceivedAndPutForHeader++] = c;

			if (c == '\n' && bytesEqualToHeaderEnding % 2 == 1) {
				if (++bytesEqualToHeaderEnding == 4) {
					debug(4, "the header ending has been found");
    				debug(4, "chars put for header in total %d", bytesReceivedAndPutForHeader);
    				debug(4, "header: \n%s", tempHeaderBuffer);
    				headerProcessed = 1;
    				parseDataFromShoutcastHeader(tempHeaderBuffer, bytesReceivedAndPutForHeader);
					debug(4, "handle the rest of the data");
					if (i + 1 < nbytes) {
						debug(4, "treating %d bytes from byte %d as audio data", nbytes - i - 1, i + 1);
						handleAudioDataChunk(data + i + 1, nbytes - i - 1);
					}
					explain(data, nbytes);
    				break;
				}
			} else if (c == '\r') {
				if (bytesEqualToHeaderEnding == 2) {
					bytesEqualToHeaderEnding = 3;
				} else {
					bytesEqualToHeaderEnding = 1;
				}
			} else {
				bytesEqualToHeaderEnding = 0;
			}

			if (bytesReceivedAndPutForHeader == MAX_HEADER) {
				fatal("shoutcast header has exceeded the length limit %d", MAX_HEADER);
			}
    	}
    } else {
    	debug(3, "header is already processed, handling audio data");
    	handleAudioDataChunk(data, nbytes);
    }
}

int sock;

void initializeTCPShoutcastStreaming(char * host, char * requestPath, FILE * destination, int port, int metadataDemand) {
	struct addrinfo addr_hints;
	struct addrinfo *addr_result;
	int err;
	ssize_t len;

	debug(0, "initializing TCP communication");

	settingMetaData = metadataDemand;
	settingHost = host;
	settingPath = requestPath;
	sprintf(settingPort, "%d", port);
	settingDataDestination = destination;
	memset(streamTitle, 0, sizeof(streamTitle));

	// 'converting' host/port in string to struct addrinfo
	memset(&addr_hints, 0, sizeof(struct addrinfo));
	addr_hints.ai_family = AF_INET; // IPv4
	addr_hints.ai_socktype = SOCK_STREAM;
	addr_hints.ai_protocol = IPPROTO_TCP;

	// err = getaddrinfo("stream3.polskieradio.pl", "8900", &addr_hints, &addr_result);
	debug(2, "host %s, port %s", settingHost, settingPort);
	err = getaddrinfo(settingHost, settingPort, &addr_hints, &addr_result);
	if (err == EAI_SYSTEM) { // system error
		syserr("getaddrinfo: %s", gai_strerror(err));
	} else if (err != 0) { // other error (host not found, etc.)
		fatal("getaddrinfo: %s", gai_strerror(err));
	}

	// initialize socket according to getaddrinfo results
	sock = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
	if (sock < 0)
		syserr("socket");

	// set receive timeout to the socket
	struct timeval tv;
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval)) < 0) {
		syserr("setsockopt for the tcp socket timeout");
	}

	debug(1, "connecting to the shoutcast server");
	if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) < 0)
		syserr("connect");

	freeaddrinfo(addr_result);

	char request[BUFFER_SIZE];
	sprintf(request, REQUEST_TEMPLATE, settingPath, settingHost, settingMetaData);
	len = strnlen(request, BUFFER_SIZE);
	debug(1, "writing to socket: %s", request);
	if (write(sock, request, len) != len) {
		syserr("partial / failed write");
	}

	memset(tempHeaderBuffer, 0, sizeof(tempHeaderBuffer));
}

int handleIncomingTCPShoutcastData(int playing) {
	char buffer[BUFFER_SIZE];
	ssize_t rcv_len;

	forwardingData = playing;

	memset(buffer, 0, sizeof(buffer));
    debug(1, "reading from TCP socket");
    rcv_len = read(sock, buffer, sizeof(buffer) - 1);
    if (rcv_len < 0) {
    	if (errno == EAGAIN || errno == EWOULDBLOCK) {
    		debug(1, "timeout for read was reached, exiting the program with error");
    	}
    	syserr("read");
    } else if (rcv_len == 0) {
    	debug(1, "connection was closed by the shoutcast server, exiting the program");
    	return 0;
    }
    debug(1, "read from TCP socket: %zd bytes", rcv_len);
    processReceivedData(buffer, rcv_len);
    // explain(buffer, rcv_len);
    return rcv_len;
}

void closeTCPConnection(){
	debug(1, "closing socket of TCP connection");
	if (close(sock) < 0) {
		syserr("close socket of TCP connection");
	}
}