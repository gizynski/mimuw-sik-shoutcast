# SHOUTcast odtwarzacz na SIK 2016

## Co jeszcze nie działa
* "należy przyjąć maksymalny czas oczekiwania na zbudowanie połączenia TCP wynikający z implementacji interfejsu gniazd". Nie jestem pewien czy dobrze zrozumiałem, ale zakładam że chodziło o nieinterweniowanie w domyślny timeout przy wykonywaniu connect. Sprawdzałem, dopiero po 127 sekundach leci error `(110; Connection timed out)`, który sprawia że program kończy się z błędem. Informacje za [connect(2)](http://man7.org/linux/man-pages/man2/connect.2.html):
  >ETIMEDOUT
  >Timeout while attempting connection.  The server may be too
  >busy to accept new connections.  Note that for IP sockets the
  >timeout may be very long when syncookies are enabled on the
  >server.

## Jak co działa - przyjęte rozwiązania
* polecenie TITLE dostaje w odpowiedzi pusty datagram UDP jeśli tytuł nie jest znany, co może wynikać z:
    * braku ustawienia md na yes w wywołaniu programu
    * nieotrzymanego segmentu z metadanymi od serwera shoutcast
    * otrzymanie jedynie pustego segmentu metadanych
    * otrzymanie segmentu metadanych z pustym streamTitle
* polecenie PAUSE powoduje porzucanie informacji od serwera shoutcast (nie są przekazywane do destination)
* polecenie TITLE w momencie gdy wcześniej zostało przesłane PAUSE, będzie zwracać najnowszy znany tytuł, ponieważ pakiety są cały czas przetwarzane, mimo wstrzymania przekazywania danych na wyjście
* dajemy szansę serwerowi który odpowiada z `HTTP/1.0 200 OK` lub `HTTP/1.1 200 OK` zamiast `ICY 200 OK`
* jeśli przekroczony zostanie timeout (5 sekund) na otrzymanie wiadomości z serwera shoutcast, to program kończy się z błędem
* jeśli serwer shoutcast zamknie połączenie (otrzymane 0 bajtów), to uruchamiana jest funkcja poprawnego zakończenia programu
* to samo wywoływane jest przy otrzymaniu polecenia QUIT
* komunikacja po UDP odbywa się w nieblokujący dla programu sposób (brak otrzymania polecenia nie wywołuje oczekiwania)
* sprawdzanie czy został otrzymany komunikat UDP odbywa się po każdym otrzymaniu danych od serwera shoutcast
* jeśli zostało w parametrze wybrane zapisywanie do pliku, a nastąpi jakiś błąd to wynikowy plik jest pusty, ale jest tworzony
* przyjąłem ograniczenie na maksymalną długość sensownego headera w wysokości 5000 bajtów

## Testowanie
Informacje przydatne przy testowaniu programu.

### Uruchamianie
```
./player stream3.polskieradio.pl / 8900 testPolskieRadio.mp3 1480 yes
./player stream3.polskieradio.pl / 8900 - 1480 no | cvlc -
# Antyradio ze streamTitle w metadanych
./player ant-waw-01.cdn.eurozet.pl / 8602 - 1480 yes | mplayer -cache 300 -cache-min 1 -
# Zagraniczna stacja ORF Ö3 ze streamTitle w metadanych
./player mp3stream7.apasf.apa.at / 8000 - 1480 yes | mplayer -cache 500 -cache-min 3 -
# Inny zasób niż samo '/'
./player panel.nadaje.com /radiokatowice 9212 - 1480 yes | mplayer -cache 3000 -cache-min 5 -
./player waw01.ic2.scdn.smcloud.net /t042-1.mp3 80 - 1480 yes | cvlc -
# Odpowiedź HTTP OK zamiast ICY OK
./player shoutcast01.edpnet.net / 10150 - 1480 no | mplayer -cache 500 -cache-min 3 -
```

Cache jest potrzebny mplayerowi, bez tego ani rusz. Minus na końcu oznacza, że chcemy odtwarzać muzykę z stdin.
Opcja `-cache-min 10` jest rozsądna, chwilę trwa przygotowanie, ale odtwarza później bez zarzutu.
Więcej o opcjach mplayera można poczytać na http://www.mplayerhq.hu/DOCS/man/en/mplayer.1.html.

Minimalny działający (choć nie zawsze) zestaw opcji dla mplayera: `mplayer -cache 300 -cache-min 1 -`

### Problemy

#### Błąd free() w mplayerze

Z jakiegoś powodu, stream dla polskiego radia nie chce działać tak jak powinien. Odpalam tak:
```
./player stream3.polskieradio.pl / 8900 - 1480 no | mplayer -cache 500 -cache-min 3 -
```
Ale niestety dostaję w wyniku ``Error in `mplayer': free(): invalid pointer...``.
Nie wydaje mi się żebym robił cokolwiek źle - jeśli ustawię zapisywanie do pliku .mp3, to mplayer radzi sobie potem bez problemu z jego odtworzeniem.
Poświęciłem dużo czasu na debugowanie tej sytuacji, analizę otrzymywanych chunków, ale nie jestem w stanie powiedzieć w czym problem.
Dzieje się tak dla wszystkich stacji które sprawdziłem ze `stream3.polskie.radio.pl` - `8916`, `8080`, etc.

> Playing -.
> Reading from stdin...
> Cache size set to 3000 KiB
> Cache fill:  4.13% (126976 bytes)   
> Detected file format: MP2/3 (MPEG audio layer 2/3) (libavformat)
> *** Error in `mplayer': free(): invalid pointer: 0x0000000000c84f58 ***
> 
> 
> MPlayer interrupted by signal 6 in module: demux_open
> - MPlayer crashed. This shouldn't happen.
>   It can be a bug in the MPlayer code _or_ in your drivers _or_ in your
>   gcc version. If you think it's MPlayer's fault, please read
>   DOCS/HTML/en/bugreports.html and follow the instructions there. We can't and
>   won't help unless you provide this information when reporting a possible bug.

Ale! Daje się odtworzyć polskie radio za pomocą vlc, nie pojawia się tam ten problem. Co utwierdza mnie w przekonaniu, że błąd w tym wypadku nie leży po mojej stronie. Przykład:

```
./player stream3.polskieradio.pl / 8900 - 1480 no | cvlc -
```

#### Cache not filling - komunikat z mplayera

```
./player panel.nadaje.com /radiokatowice 9212 - 1480 yes | mplayer -cache 3000 -cache-min 5 -
```
Rzuca `Cache not filling, consider increasing -cache and/or -cache-min!` jak przekroczymy `Cache fill:  4.93% (151552 bytes`, ale po chwili cierpliwości zaczyna normalnie grać.

#### Odpowiedź w postaci zgodnej z HTTP zamiast z ICY

```
./player stream01.energy.at / 8000 - 1480 no | mplayer -cache 500 -cache-min 3 -
```
Zwraca `HTTP/1.0 200 OK\r\nContent-Type: text/html\r\nContent-Length: 1075\r\n\r\n`

#### Odpowiedź ICY ze statusem HTTP OK zamiast ICY OK

```
./player shoutcast01.edpnet.net / 10150 - 1480 no | mplayer -cache 500 -cache-min 3 -
```
Zwraca `HTTP/1.0 200 OK`, a reszta headera zgodnie z ICY. Dodałem w kodzie obsługę dla takiej sytuacji.
Jeśli ma być zapisywane do pliku to działa normalnie. W przypadku live streamu znowu miałem komunikat ``Error in `mplayer': free(): invalid pointer...``

### Przesyłanie poleceń do programu
```
nc -u {host playera} {m-port}
```
Kończenie datagramu `[CTRL]+[D]`