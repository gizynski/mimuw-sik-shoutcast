/*
 *    Wojciech Gizynski 264887
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "err.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

extern int sys_nerr;

void setTextColor(int attr, int fg, int bg, char* colorString);
void formatErrString(int level, char errString[], char errType[]);
void printInColorErrString(char errstring[], char textColorString[], int reversed);

void syserr(const char *fmt, ...)  
{
    va_list fmt_args;

    char errstring[2048];
    formatErrString(0, errstring, "ERROR");
    
    va_start(fmt_args, fmt);
        char errarg[1024];
        vsprintf(errarg, fmt, fmt_args);
        strcat(errstring, errarg);
    va_end (fmt_args);

    char errsignature[1024];
    sprintf(errsignature, " (%d; %s)\n", errno, strerror(errno));
    strcat(errstring, errsignature);
  
    printInColorErrString(errstring, ANSI_COLOR_RED, 1);
    exit(1);
}

void fatal(const char *fmt, ...)
{
    va_list fmt_args;

    char errstring[2048];
    formatErrString(0, errstring, "FATAL");

    va_start(fmt_args, fmt);
        char errarg[1024];
        vsprintf(errarg, fmt, fmt_args);
        strcat(errstring, errarg);
    va_end (fmt_args);

    printInColorErrString(errstring, ANSI_COLOR_RED, 1);
    exit(1);
}

void debug(int level, const char *fmt, ...) {
    #ifdef DEBUG 
        va_list fmt_args;
        
        char errstring[2048];
        formatErrString(level, errstring, "DEBUG");
        
        va_start(fmt_args, fmt);
            char errarg[1024];
            vsprintf(errarg, fmt, fmt_args);
            strcat(errstring, errarg);
        va_end (fmt_args);
        
        /* fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __FUNCTION__, ##args); */
        
        printInColorErrString(errstring, ANSI_COLOR_GREEN, 0);
    #endif
}

void info(int level, const char *fmt, ...) {
    #ifdef INFO
        va_list fmt_args;
        
        char errstring[2048];
        formatErrString(level, errstring, "INFO");
        
        va_start(fmt_args, fmt);
            char errarg[1024];
            vsprintf(errarg, fmt, fmt_args);
            strcat(errstring, errarg);
        va_end (fmt_args);
        
        printInColorErrString(errstring, ANSI_COLOR_YELLOW, 0);
    #endif
}

void formatErrString(int level, char errstring[], char errType[]) {
    char indentstring[1024] = "";
    #ifdef DEBUG    
        int i;
        for (i = 0; i < level; i++) {
            strcat(indentstring, "    "); 
        }
    #endif
    sprintf(errstring, " %5s: %s", errType, indentstring);
}

void printInColorErrString(char errstring[], char textColorString[], int reversed) {
    #ifdef DEBUG
        char textColorResetString[20];
        sprintf(textColorResetString, "%s", ANSI_COLOR_RESET);
        fprintf(stderr, "%s%s%s\n", textColorString, errstring, textColorResetString);
    #else
        fprintf(stderr, "%s\n", errstring);
    #endif
}
