/*
 *    Wojciech Gizynski 264887
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "err.h"
#include "commands.h"
#include "utils.h"

#define BUFFER_SIZE         10
#define STRING_LIMIT        256
#define METADATA_LIMIT      16 * 255

int socketForUDPServer;
struct sockaddr_in udpServerAddress;
struct sockaddr_in udpClientAddress;
socklen_t udpServerAddressLength, udpSenderAddressLength, udpReceiverAddressLength;
char title[METADATA_LIMIT];

void setNonBlockingSocket(int socketNumber) {
	int on = 1;
    ioctl(socketNumber, FIONBIO, &on);
}

void setTitleResponse(char * str) {
    sprintf(title, "%s", str);
}

int recognizeCommandFromString(char * str) {
    char * commandStrings[] = {"", PAUSE_STRING, PLAY_STRING, TITLE_STRING, QUIT_STRING};
    int recognizedCommand = NO_COMMAND;
    
    #ifdef DEBUG
        int len = strlen(str) - 1;

        while (len >= 0) {
            int charCode = (int) str[len];
            if (charCode < 32 || charCode == 127) {
                str[len] = '\0';
                len--;
            } else {
                break;
            }
        }
    #endif

    int i;
    for(i = 1; i < 5; i++) {
        debug(3, "comparing strings '%s' and '%s'", str, commandStrings[i]);
        if (strcmp(str, commandStrings[i]) == 0) {
            recognizedCommand = i;
            break;
        }
    }
    if (recognizedCommand) {
        debug(3, "command recognized: %s - %d", str, recognizedCommand);
    }
    return recognizedCommand;
}

void initializeUDPCommandHandling(int portNumber) {
    debug(0, "initializing UDP communication");
    debug(1, "setting socketForUDPServer");
    socketForUDPServer = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketForUDPServer < 0)
        syserr("socket for UDP server");

    udpServerAddress.sin_family = AF_INET;
    udpServerAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    udpServerAddress.sin_port = htons(portNumber);
    udpServerAddressLength = (socklen_t) sizeof(udpServerAddress);

    memset(title, 0, sizeof(title));

    setNonBlockingSocket(socketForUDPServer);

    debug(1, "setting bind");
    int bindResult = bind(socketForUDPServer, (struct sockaddr *) &udpServerAddress, udpServerAddressLength);
    if (bindResult < 0)
        syserr("bind for UDP server socket");

    udpSenderAddressLength = (socklen_t) sizeof(udpClientAddress);
}

int handleIncomingUDPCommand() {
    char buffer[BUFFER_SIZE];
    ssize_t bytesReceived, bytesSent, bytesToSend;
    int receivedCommand = NO_COMMAND;

    udpReceiverAddressLength = (socklen_t) sizeof(udpClientAddress);
    int flags = 0;
    memset(buffer, 0, sizeof(buffer));
    bytesReceived = recvfrom(socketForUDPServer, buffer, sizeof(buffer) - 1, flags, (struct sockaddr *) &udpClientAddress, &udpReceiverAddressLength);
    buffer[bytesReceived] = '\0';
    // explain(buffer, bytesReceived);
    if (bytesReceived < 0) {
        if (errno == EAGAIN) {
            debug(2, "no commands to process at the moment");
        } else {
            info(0, "error on datagram from client socket");
        }
    } else {
        debug(2, "read from socket: %zd bytes: %.*s", bytesReceived, (int) bytesReceived, buffer);
        receivedCommand = recognizeCommandFromString(buffer);
        debug(2, "received command %d", receivedCommand);
        if (receivedCommand == NO_COMMAND) {
            info(2, "received message is not a valid command - try one of PAUSE/PLAY/TITLE/QUIT");
        }
        if (receivedCommand == TITLE) {
            debug(2, "title is %s", title);
            int sflags = 0;
            bytesToSend = strnlen(title, METADATA_LIMIT);
            bytesSent = sendto(socketForUDPServer, title, bytesToSend, sflags, (struct sockaddr *) &udpClientAddress, udpSenderAddressLength);
            if (bytesSent != bytesToSend) {
                info(2, "error on sending datagram to client socket");
            }
        }
    }
    return receivedCommand;
}

void closeUDPConnection(){
    debug(1, "closing socket of UDP connection");
    if (close(socketForUDPServer) < 0) {
        syserr("close socket of TCP connection, file descriptor %d", socketForUDPServer);
    }
}