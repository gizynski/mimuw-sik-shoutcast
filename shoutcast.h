/*
 *    Wojciech Gizynski 264887
 */

#ifndef _SHOUTCAST_
#define _SHOUTCAST_


/* Rozpocznij komunikacje z serwerem Shoutcasta */
extern void initializeTCPShoutcastStreaming(char * host, char * requestPath, FILE * destination, int port, int metadataDemand);

/* Odbierz dane od serwera Shoutcasta i obsluz je */
extern int handleIncomingTCPShoutcastData(int forwardingData);

/* Zamknij socket uzywany do komunikacji z serwerem shoutcasta */
extern void closeTCPConnection();

#endif 